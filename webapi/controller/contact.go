/*
	controller handlers web request, validates parameters, and encode/decode data between user input and internal service
*/

package controller

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenyukgo/autopilot/webapi/businesslogic"
	"gitlab.com/zenyukgo/autopilot/webapi/model"
)

var businessLogicClient businesslogic.ContactBusinessLogicClient

// SetupContactController - re
// not using init() because it's complicates testing
func SetupContactController(redisConn redis.Conn) {
	businessLogicClient = businesslogic.NewContactBusinessLogicClient(redisConn)
}

// GetContactByID - returns Autopilot contact entity by contact_id
func GetContactByID(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), 30*time.Second)
	defer cancel()

	httpParams := mux.Vars(r)
	contactID := httpParams["contactID"]
	log.Info("controller - GetContactByID called with ID: ", contactID)
	if contactID == "" || len(contactID) > 43 {
		http.Error(w, "invalid request parameter - contact_id", 400)
		return
	}

	contact, err := businessLogicClient.GetContact(ctx, contactID)
	if err != nil {
		http.Error(w, "internal server error: "+err.Error(), 500)
		return
	}
	if contact == nil || contact.ContactID == "" {
		http.Error(w, "could NOT find existing contact for a given ID", 404)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode(contact)
}

// CreateContact - creates Autopile contact
func CreateContact(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), 30*time.Second)
	defer cancel()

	var contact model.ContactMinimized
	_ = json.NewDecoder(r.Body).Decode(&contact)
	log.Info("controller - CreateContact called with params: ", contact)
	contactID, err := businessLogicClient.CreateContact(ctx, &contact)
	if err != nil || contactID == "" {
		http.Error(w, "internal server error: "+err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode("{'contact_id': '" + contactID + "'}")
}

// UpdateContact - updates existing Autopilot contact
func UpdateContact(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(r.Context(), 30*time.Second)
	defer cancel()

	httpParams := mux.Vars(r)
	contactID := httpParams["contactID"]
	log.Info("controller - UpdateContact called with ID: ", contactID)
	if contactID == "" || len(contactID) > 43 {
		http.Error(w, "invalid request parameter - contact_id", 400)
		return
	}

	var contact model.ContactMinimized
	_ = json.NewDecoder(r.Body).Decode(&contact)
	ok, newContactID, err := businessLogicClient.UpdateContact(ctx, contactID, &contact)
	if err != nil || !ok {
		http.Error(w, "internal server error: "+err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	if newContactID == "" {
		encoder.Encode("{'result': 'success'}")
	} else {
		w.WriteHeader(http.StatusCreated)
		encoder.Encode("{'contact_id': '" + newContactID + "'}")
	}
}

// Close - closes connections to Redis
func Close() {
	businessLogicClient.CloseRedisConnection()
}
