package model

// Contact - Autopilot entity representing a person in contact
type Contact struct {
	ContactID     string        `json:"contact_id"`
	Email         string        `json:"Email"`
	Name          string        `json:"Name"`
	FirstName     string        `json:"FirstName"`
	LastName      string        `json:"LastName"`
	CreatedAt     string        `json:"created_at"`
	UpdatedAt     string        `json:"updated_at"`
	APIOriginated bool          `json:"api_originated"`
	CustomFields  []CustomField `json:"custom_fields"`
	//TODO: add the rest of the fields
}
