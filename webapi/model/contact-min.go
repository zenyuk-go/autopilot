package model

// ContactMinimized - entity used for creating a new contact
type ContactMinimized struct {
	Email     string `json:"Email"`
	FirstName string `json:"FirstName"`
	LastName  string `json:"LastName"`
}
