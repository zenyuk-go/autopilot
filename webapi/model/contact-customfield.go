package model

// CustomField - Autopilot entity representing a contact's custom field
type CustomField struct {
	Kind      string `json:"kind"`
	Value     string `json:"value"`
	FieldType string `json:"fieldType"`
	Deleted   bool   `json:"deleted"`
}
