package main

import (
	"net/http"

	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenyukgo/autopilot/webapi/controller"
)

const (
	httpPort  = ":8080"
	redisPort = ":6379"
)

func main() {
	defer controller.Close()

	log.SetLevel(log.InfoLevel)

	redisConnection, err := redis.Dial("tcp", "rediscache"+redisPort)
	if err != nil {
		log.Fatal("Can NOT establish connection to Redis for caching purposes. Exiting. ", err)
	}
	controller.SetupContactController(redisConnection)

	router := mux.NewRouter()
	router.HandleFunc("/contact/{contactID}", controller.GetContactByID).Methods("GET")
	router.HandleFunc("/contact/{contactID}", controller.UpdateContact).Methods("PUT")
	router.HandleFunc("/contact", controller.CreateContact).Methods("POST")
	router.HandleFunc("/contact/", controller.CreateContact).Methods("POST")
	http.ListenAndServe(httpPort, router)

	log.Info("Web API started on port ", httpPort)
	select {}
}
