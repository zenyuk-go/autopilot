package cache

import (
	"encoding/json"
	"errors"
	"strconv"

	log "github.com/sirupsen/logrus"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/zenyukgo/autopilot/webapi/model"
)

// GetContactByID - retrieves Contact Autopilot entity from cache if exists
func GetContactByID(connection redis.Conn, contactID string) (*model.Contact, error) {
	log.Debug("cache - GetContactByID - contactID: ", contactID)
	values, err := redis.Strings(connection.Do("HMGET", "contact:"+contactID, "contact_id", "Email", "Name", "FirstName", "LastName", "CreatedAt", "UpdatedAt", "APIOriginated", "CustomFields"))
	if err != nil || values == nil || len(values) < 9 {
		log.Error(err)
		return nil, errors.New("can NOT get Contact from cache")
	}
	if values[0] != contactID {
		// cache missed
		return nil, nil
	}

	apiOrig, err := strconv.ParseBool(values[7])
	if err != nil {
		log.Error("cache - GetContactByID - can't parse api_originated field")
		return nil, errors.New("can NOT parse bool field api_originated")
	}

	customFields, err := parseCustomFields(values[8])
	if err != nil {
		log.Error("cache - GetContactByID - can't parse custom fields")
		return nil, errors.New("can NOT parse custom fields of a Contact")
	}

	// not using redis.ScanStruct for expliciticy
	var contact model.Contact
	contact.ContactID = values[0]
	contact.Email = values[1]
	contact.Name = values[2]
	contact.FirstName = values[3]
	contact.LastName = values[4]
	contact.CreatedAt = values[5]
	contact.UpdatedAt = values[6]
	contact.APIOriginated = apiOrig
	contact.CustomFields = customFields

	log.Debug("cache - GetContactByID - result contact: ", contact)

	return &contact, nil
}

// StoreContact - saves Contact Autopilot entity in cache
// invalidates existing cache and extends lifetime for an existing record
func StoreContact(connection redis.Conn, contact *model.Contact) (bool, error) {
	log.Debug("cache - StoreContact - contact: ", contact)

	rawBytes, err := json.Marshal(contact.CustomFields)
	if err != nil {
		log.Error("Can't marshal contact.CustomFields to json, ", err)
		return false, errors.New("can NOT store Contact in Redis cache")
	}
	jsonCustomFields := string(rawBytes)

	_, err = connection.Do("HMSET", "contact:"+contact.ContactID,
		"contact_id", contact.ContactID,
		"Email", contact.Email,
		"Name", contact.Name,
		"FirstName", contact.FirstName,
		"LastName", contact.LastName,
		"CreatedAt", contact.CreatedAt,
		"UpdatedAt", contact.UpdatedAt,
		"APIOriginated", contact.APIOriginated,
		"CustomFields", jsonCustomFields)
	if err != nil {
		log.Error(err)
		return false, errors.New("can NOT store Contact in Redis cache")
	}
	return true, nil
}

func parseCustomFields(rawJSON string) ([]model.CustomField, error) {
	customFields := []model.CustomField{}
	log.Debug("cache - parseCustomFields - rawJSON: ", rawJSON)
	err := json.Unmarshal([]byte(rawJSON), &customFields)
	if err != nil {
		log.Error("cache - parseCustomFields - un-marshalling error: ", err.Error())
		return nil, err
	}

	return customFields, nil
}
