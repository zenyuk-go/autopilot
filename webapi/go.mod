module gitlab.com/zenyukgo/autopilot/webapi

go 1.14

require (
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/sirupsen/logrus v1.5.0
)
