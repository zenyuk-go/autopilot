/*
	businesslogic makes all necessary business manipulations with model Entities
*/

package businesslogic

import (
	"context"
	"errors"

	"github.com/gomodule/redigo/redis"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenyukgo/autopilot/webapi/autopilotclient"
	"gitlab.com/zenyukgo/autopilot/webapi/cache"
	"gitlab.com/zenyukgo/autopilot/webapi/model"
)

// ContactBusinessLogicClient - interface to call the Business Logic layer for Contact CRUD operations
type ContactBusinessLogicClient interface {
	CloseRedisConnection()
	GetContact(ctx context.Context, contactID string) (*model.Contact, error)
	CreateContact(ctx context.Context, contact *model.ContactMinimized) (string, error)
	UpdateContact(ctx context.Context, contactID string, contact *model.ContactMinimized) (bool, string, error)
}

type contactBusinessLogicClient struct {
	redisConnection redis.Conn
}

// NewContactBusinessLogicClient - constructs a client to call the Business Logic layer for Contact entity purposes
func NewContactBusinessLogicClient(redisConn redis.Conn) ContactBusinessLogicClient {
	return &contactBusinessLogicClient{redisConn}
}

// GetContact - returns Contact entity
func (client *contactBusinessLogicClient) GetContact(ctx context.Context, contactID string) (*model.Contact, error) {
	_, contextIsAlive := ctx.Deadline()
	if !contextIsAlive {
		return nil, errors.New("context timed out")
	}

	// first trying to get from the cache
	contact, err := cache.GetContactByID(client.redisConnection, contactID)
	if err != nil {
		log.Error("businesslogic - GetContact - error while trying to get Contact from cache. ", err)
	}
	if contact != nil {
		return contact, nil
	}

	// getting from Autopilot API, when cache missed
	contact, errFromAPI := autopilotclient.GetContactByID(contactID)
	if errFromAPI != nil {
		log.Error("businesslogic - GetContact - error while trying to get Contact from Autopilot API")
		return nil, errors.New("error communicating to Autopilot API")
	}

	// store in cache
	ok, err := cache.StoreContact(client.redisConnection, contact)
	if err != nil || !ok {
		log.Error("businesslogic - GetContact - error while trying to store Contact in cache. ", err)
	}

	return contact, nil
}

func (client *contactBusinessLogicClient) CreateContact(ctx context.Context, contactMin *model.ContactMinimized) (string, error) {
	contactID, errFromAPI := autopilotclient.CreateContact(contactMin)
	if errFromAPI != nil || contactID == "" {
		log.Error("businesslogic - CreateContact - error while trying to create Contact in Autopilot API")
		return "", errors.New("error communicating to Autopilot API")
	}

	log.Info("businesslogic - CreateContact - new contact created with ID: ", contactID)

	// call Autopilot to get full version of the entity to cache it
	contact, errFromAPI := autopilotclient.GetContactByID(contactID)
	if errFromAPI != nil {
		log.Error("businesslogic - GetContact - error while trying to get Contact from Autopilot API")
		return "", errors.New("error communicating to Autopilot API")
	}

	log.Debug("businesslogic - CreateContact - new contact retrieved from API: ", contact)

	// store in cache
	ok, err := cache.StoreContact(client.redisConnection, contact)
	if err != nil || !ok {
		log.Error("businesslogic - CreateContact - error while trying to store Contact in cache. ", err)
	}

	return contactID, nil
}

// UpdateContact - updates existing contact or creates a new one.
// If a new contact was created, the second return param will have it set
func (client *contactBusinessLogicClient) UpdateContact(ctx context.Context, contactID string, contact *model.ContactMinimized) (bool, string, error) {
	ok, newContactID, errFromAPI := autopilotclient.UpdateContact(contactID, contact)
	if errFromAPI != nil || !ok {
		log.Error("businesslogic - UpdateContact - error while trying to update Contact in Autopilot API")
		return false, "", errors.New("error communicating to Autopilot API")
	}

	contactIDToBeCached := contactID
	if newContactID != "" {
		// a new contact was created instead of updating
		contactIDToBeCached = newContactID
	}

	// call Autopilot to get full version of the entity to cache it
	updatedContact, errFromAPI := autopilotclient.GetContactByID(contactIDToBeCached)
	if errFromAPI != nil {
		log.Error("businesslogic - GetContact - error while trying to get Contact from Autopilot API")
		return false, "", errors.New("error communicating to Autopilot API")
	}

	log.Debug("businesslogic - UpdateContact - new contact retrieved from API: ", updatedContact)

	// store in cache
	ok, err := cache.StoreContact(client.redisConnection, updatedContact)
	if err != nil || !ok {
		log.Error("businesslogic - UpdateContact - error while trying to store Contact in cache. ", err)
	}

	return true, newContactID, nil
}

// CloseRedisConnection - closes underlying Redis connection
func (client *contactBusinessLogicClient) CloseRedisConnection() {
	client.redisConnection.Close()
}
