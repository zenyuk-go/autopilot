/*
	HTTP client to call Autopilot web API
*/

package autopilotclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/zenyukgo/autopilot/webapi/model"
)

const (
	autopilotapikey = "bb9b564118ae411e839569c9b49166cb"
)

// custom structure to comply with one from Autopilot
type createContact struct {
	Contact model.ContactMinimized `json:"contact"`
}

type createContactResponse struct {
	ContactID string `json:"contact_id"`
}

// GetContactByID - retrieves a Contact entity from Autopilot API
func GetContactByID(contactID string) (*model.Contact, error) {
	log.Info("autopilotclient - GetContactByID - request contactID: ", contactID)
	req, err := http.NewRequest("GET", "https://api2.autopilothq.com/v1/contact/"+contactID, nil)
	if err != nil {
		log.Error("autopilotclient - GetContactByID - can NOT create request for Autopilot API, contact_id: ", contactID)
		return nil, errors.New("get contact error")
	}
	req.Header.Add("autopilotapikey", autopilotapikey)
	req.Header.Add("Content-Type", "application/json")
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		log.Error("autopilotclient - GetContactByID - can NOT get Contact from Autopilot API, ID: ", contactID)
		return nil, errors.New("get contact error")
	}
	b, err := ioutil.ReadAll(response.Body)
	defer response.Body.Close()
	if err != nil {
		log.Error("autopilotclient - GetContactByID - error reading response body ", err.Error())
		return nil, errors.New("get contact error")
	}

	contact := model.Contact{}
	rawJSON := string(b)
	log.Debug("autopilotclient - GetContactByID - raw JSON: ", rawJSON)
	err = json.Unmarshal([]byte(rawJSON), &contact)
	if err != nil {
		log.Error("autopilotclient - GetContactByID - un-marshalling error: ", err.Error())
		return nil, errors.New("get contact error")
	}

	return &contact, nil
}

// UpdateContact - updates existing Contact entity in Autopilot API
// in case a new Contact will be created instead, return param newContactID will have a value
func UpdateContact(contactID string, contact *model.ContactMinimized) (ok bool, newContactID string, err error) {
	log.Info("autopilotclient - UpdateContact - contactID: ", contactID)
	// check if exists
	existingContact, _ := GetContactByID(contactID)
	newContactID, _ = CreateContact(contact)
	if existingContact == nil || existingContact.Email != contact.Email {
		// a new Contact was created
		return true, newContactID, nil
	}
	if newContactID == contactID {
		// existing contact was updated
		return true, "", nil
	}
	// error case if updated contact ID doesn't match one form params, but a new Contact wasn't created
	log.Error("updated contact ID doesn't match one form params, but a new Contact wasn't created")
	return false, "", errors.New("problem while updating a Contact")
}

// CreateContact - creates a Contact entity in Autopilot API
func CreateContact(contact *model.ContactMinimized) (contactID string, err error) {
	createContactParam := createContact{*contact}

	log.Debug("autopilotclient - CreateContact - createContactParam: ", createContactParam)

	req, err := http.NewRequest("POST", "https://api2.autopilothq.com/v1/contact", nil)
	if err != nil {
		log.Error("autopilotclient - CreateContact - can NOT create request for Autopilot API, contact: ", contact)
		return "", errors.New("create contact error")
	}
	req.Header.Add("autopilotapikey", autopilotapikey)
	req.Header.Add("Content-Type", "application/json")

	body, err := json.Marshal(createContactParam)
	if err != nil {
		log.Error("autopilotclient - UpdateContact - marshalling Contact. ", err)
		return "", errors.New("can't marshal Contact")
	}
	req.Body = ioutil.NopCloser(bytes.NewReader(body))
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		log.Error("autopilotclient - CreateContact - can NOT post Contact to Autopilot API: ", contact)
		return "", errors.New("create contact error")
	}
	defer response.Body.Close()

	// get new Contact ID
	rspWithContactID := createContactResponse{}
	b, err := ioutil.ReadAll(response.Body)
	rawJSON := string(b)

	log.Debug("autopilotclient - CreateContact - rawJSON response: ", rawJSON)

	err = json.Unmarshal([]byte(rawJSON), &rspWithContactID)
	if err != nil {
		log.Error("autopilotclient - CreateContact - un-marshalling error: ", err.Error())
		return "", errors.New("create contact error")
	}

	return rspWithContactID.ContactID, nil
}
