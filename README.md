# autopilot

architecture diagram [](https://photos.app.goo.gl/JiMBzRmjrSRkZW5j7)
https://photos.app.goo.gl/BRbwMCboR5gbjpRH8

As one can see from the diagram, the solution consists of these main parts.
Browser request are coming to the implemented service, the service calls to 
Redis to get/store cached Entities, and the service also calls Autopilot API.

For HTTP GET request the service first tries to get Entity from Redis cache and 
in case of cache miss, it will call the Autopilot API. If call to Autopilot API 
was successful, the cache will be populated with the Entity.

# packages

The Service itself consist of few parts split into packages. First package
that handles the browser requests is called "controller", user input is 
converted into entities which are stored in "model" packages. Controller performs
user input validation and converting internal error into correct HTTP codes.
Next come "business logic layer" where application decides how to further handle
the user request. Like call cache or call external API. All those packages complete
MVC pattern. 

Calls to external API like Autopilot API are put into separate package - "autopilotclient".
And calls to redis server, which runs as a separate docker container, are
happening inside the "cache" package. 

Inside packages we can see files like "contact.go" which name reflects the entity 
file is designed to work with. It's how we split package into parts by files, 
each file for it's entity.

# how to run

In total we have 2 docker containers: the service and Redis server. To run the 
solution, we need to to the root directory and run "docker-compose up". Docker
and docker-compose must be installed. 

# how to test

To test get Contact, open in browser and hit
`http://127.0.0.1:8080/contact/person_BFC18F76-EB3C-4AFA-9ED1-9CFA22E080C6`
Try twice, the second try will not miss the cache and will not call Autopilot API.

To test create Contact, execute in terminal:
`curl -i -H "Content-Type: application/json" -H "autopilotapikey: bb9b564118ae411e839569c9b49166cb" --request POST -d '{"FirstName": "Jon", "LastName": "Dow", "Email": "posty@post.com"}' http://127.0.0.1:8080/contact`

The create request result will be stored in cache. You can see it by making 
another GET call. Copy the returned contact_id and paste in the first browser 
request or use curl with updated contact_id

To test update Contact, fist create a new one (see above how) and execute in terminal:
replacing `person_6307A542-BB22-4767-BB49-0B295D0609BF` with your Contact ID:
`curl -i -H "Content-Type: application/json" -H "autopilotapikey: bb9b564118ae411e839569c9b49166cb" --request PUT -d '{"FirstName": "Jon5", "LastName": "Dow5", "Email": "posty@post.com"}' http://127.0.0.1:8080/contact/person_6307A542-BB22-4767-BB49-0B295D0609BF`

In case you provide a new Email, a new Contact will be created by the update
method and HTTP status 201 will be returned.


# tests, no time left

Because the task suppose to take 2 hours or comparable, tests have not been
implemented yet. What would I test is following:
1) mock version of cache, comparing "contact_id" provided with one returned
2) mock version of Autopilot API, paying attention to all fields.
3) parsing (marshalling/unmarshalling) of the custom fields.
4) logic to set api_originated and updated timestamps flags
5) error cases, when entity suppose to be in the cache, it should not fail still
6) context timeout
7) missing "autopilotapikey"
8) update method returning a new Contact instead of updating existing, when emails don't match

what is not finished yet: put (update) request resets the custom fields